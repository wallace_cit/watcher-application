'use strict';

// http://webapplog.com/tdd/

const assert = require('assert');
const example = require('../../libs/example');

describe('Example module', function() {

  it('exist package.json file', function(done) {

    example.checkExistence('package.json', function(exists, text) {

      assert.equal(true, exists);
      assert.equal('exists!', text);
      done();

    });

  });

  it('do not exist upsidedown.json file', function(done) {

    example.checkExistence('upsidedown.json', function(exists, text) {

      assert.equal(false, exists);
      assert.equal('do not exists!', text);
      done();

    });

  });

});
