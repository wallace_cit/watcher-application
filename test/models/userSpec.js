'use strict';

const chai = require('chai');
const assert = chai.assert;

const User = require('../../models/user');

describe('Model User', function() {

  var user = {};

  describe('is empty, check', function() {

    before(function() {
      user = new User();
    });

    it('username is undefined', function() {
      assert.isUndefined(user.username);
    });

    it('password is undefined', function() {
      assert.isUndefined(user.password);
    });

    it('admin is undefined', function() {
      assert.isFalse(user.admin);
    });

    it('is invalid', function() {
      var error = user.validateSync();
      assert.isNotNull(error);
      assert.equal(2, Object.keys(error.errors).length);
    });

  });

  describe('is not empty, check', function() {

    before(function() {
      user = new User({
        username: 'john_doe',
        password: 'secret',
        admin: true
      });
    });

    it('username is "john_doe"', function() {
      assert.equal('john_doe', user.username);
    });

    it('password is "secret"', function() {
      assert.equal('secret', user.password);
    });

    it('admin is true', function() {
      assert.isTrue(user.admin);
    });

    it('is valid', function() {
      assert.isUndefined(user.validateSync());
    });

  });

});
