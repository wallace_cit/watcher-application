'use strict';

const express = require('express');
const expressValidator = require('express-validator');
const session = require('express-session');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const methodOverride = require('method-override');

const request = require('supertest');
const chai = require('chai');
const assert = chai.assert;

const sinon = require('sinon');
require('sinon-mongoose');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
  resave: true, saveUninitialized: true,
  secret: 'uwotm8'
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(expressValidator());
app.use(methodOverride());

app.use('/', require('../../routes/authentication'));

describe('Authentication test', function() {

  var mock = sinon.mock(mongoose.model('User'));

  describe('With valid user', function() {
    var validUser = {
      username: "admin",
      password: "admin",
      admin: "true"
    };

    before(function() {
      mock = sinon.mock(mongoose.model('User'));

      mock.expects('findOne').withArgs({
        username: validUser.username
      }).yields(null, validUser);

    });

    after(function() {
      mock.restore();
    });

    it(', with success, returning a token', function(done) {
      var token;

      request(app).get('/').query(validUser).end(function(err, res) {
        assert.isNull(err);
        assert.equal(200, res.status);
        token = res.body;
        assert.isNotNull(token);
        done();
      });
    });
  });
});
