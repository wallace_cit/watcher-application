'use strict';

const mongoose = require('mongoose');

const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');
const chai = require('chai');
const assert = chai.assert;
const jwt = require('jsonwebtoken');
const sinon = require('sinon');
require('sinon-mongoose');

var app = express();
app.use(bodyParser.json());
app.set('view engine', 'hbs');
app.use('/', require('../../routes/users'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// catch generic error handler
app.use(function(err, req, res, next) {
  if (res.headersSent) {
    return next(err);
  }

  var result = {
    message: err.message,
    status: err.status || 500,
    details: err.details
  };

  if (app.get('env') === 'development') {
    result.stack = err.stack;
  }

  res.status(result.status);

  var contype = req.headers['content-type'];
  if (!contype || contype.indexOf('application/json') !== 0) {
    res.render('error', result);
  } else {
    res.json(result);
  }

});

describe('Route user', function() {

  var mock = {};
  var jwtMock;

  var validUser = {
    username: 'test',
    password: 'test',
    admin: true
  };

  before(function() {
    mock = sinon.mock(mongoose.model('User'));

    jwtMock = sinon.mock(jwt);
    jwtMock.expects('verify').yields(null, validUser);
  });

  after(function() {
    mock.restore();
    jwtMock.restore();
  });

  describe('/user', function() {

    it('return null if GET /', function(done) {

      mock.expects('find').yields(null, []);
      jwtMock.expects('verify').yields(null, validUser);

      request(app).get('/').query({ token: 'a' }).end(function(err, res) {

        assert.isNull(err);
        assert.equal('', res.text);
        assert.equal(204, res.status);

        done();

      });

    });

    it('return null if DELETE /', function(done) {

      mock.expects('remove').yields(null, []);
      jwtMock.expects('verify').yields(null, validUser);

      request(app).delete('/').query({ token: 'a' }).end(function(err, res) {

        assert.isNull(err);
        assert.equal('', res.text);
        assert.equal(204, res.status);

        done();

      });

    });

    it('return {} if valid POST /', function(done) {

      var user = { username: 'JohnDoe', password: '123456' };

      mock.expects('create').withArgs(user).yields(null, user);
      jwtMock.expects('verify').yields(null, user);

      request(app).post('/').query({ token: 'a' }).send(user).end(function(err, res) {

        assert.isNull(err);
        assert.equal(JSON.stringify(user), res.text);
        assert.equal(200, res.status);

        done();

      });

    });

    it('return error if invalid POST /', function(done) {

      var user = {};
      mock.expects('create').withArgs(user).yields(new Error('User validation failed'), null);
      jwtMock.expects('verify').yields(null, user);

      request(app).post('/').query({ token: 'a' }).send(user).end(function(err, res) {

        assert.isNull(err);
        assert.equal(500, res.status);
        assert.include(res.text, 'User validation failed');

        done();

      });

    });

    it('return error if invalid GET /:userId', function(done) {

      var user = {};

      jwtMock.expects('verify').yields(null, user);

      request(app).get('/0').query({ token: 'a' }).send(user).end(function(err, res) {
        assert.isNull(err);
        assert.equal(500, res.status);

        done();

      });

    });

    it('return error if invalid PUT /:userId', function(done) {

      var user = {};
      jwtMock.expects('verify').yields(null, user);

      request(app).put('/0').query({ token: 'a' }).send(user).end(function(err, res) {
        assert.isNull(err);
        assert.equal(500, res.status);

        done();

      });

    });

    it('return error if invalid DELETE /:userId', function(done) {

      var user = {};
      jwtMock.expects('verify').yields(null, user);

      request(app).delete('/0').query({ token: 'a' }).send(user).end(function(err, res) {
        assert.isNull(err);
        assert.equal(500, res.status);

        done();

      });

    });

    it('call findById method with valid GET /:userId', function(done) {
      mock.restore();
      var id = "abc";
      var result = "OK";
      var user = {};

      jwtMock.expects('verify').yields(null, user);
      mock.expects('findById').withArgs(id).yields(null, result);

      request(app).get('/' + id).query({ token: 'a' }).send().end(function(err, res) {
        assert.isNull(err);
        assert.equal(res.body, result);
        done();
      });

    });

    it('call findByIdAndRemove method with valid DELETE /:userId', function(done) {
      mock.restore();
      var id = "abc";
      var result = "OK";
      var user = {};

      jwtMock.expects('verify').yields(null, user);
      mock.expects('findByIdAndRemove').withArgs(id).yields(null, result);

      request(app).delete('/' + id).query({ token: 'a' }).send().end(function(err, res) {
        assert.isNull(err);
        assert.equal(res.body, result);
        done();
      });

    });

  });

});

describe('Token Verification', function() {

  var mock = {};
  var jwtMock;

  var validUser = {
    username: 'test',
    password: 'test',
    admin: true
  };

  before(function() {
    mock = sinon.mock(mongoose.model('User'));

    jwtMock = sinon.mock(jwt);
    jwtMock.expects('verify').yields(null, validUser);
  });

  after(function() {
    mock.restore();
    jwtMock.restore();
  });

  describe('/user', function() {

    it.only('return error, if the authenticated user doesnt exist in the database', function(done) {

      mock.expects('findOne').yields(new Error('Usuario nao encontrado'), null);
      jwtMock.expects('verify').yields(null, validUser);

      request(app).get('/').query({ token: 'a' }).end(function(err, res) {

        assert.isNull(err);
        assert.isNotNull(res.error);
        assert.equal(401, res.status);

        done();

      });

    });
  });
});
