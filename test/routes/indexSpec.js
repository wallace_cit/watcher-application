'use strict';

const hbs = require('hbs');
var path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const request = require('supertest');
const chai = require('chai');
require('chai').should();
const assert = chai.assert;
const sinon = require('sinon');
const userService = require('../../services/userService');

var app = express();

app.use(bodyParser.json());
app.set('views', path.resolve(__dirname, '../../views'));
app.set('view engine', 'hbs');
app.use('/', require('../../routes/index'));

describe('Route index', function() {

  describe('/', function() {

    it('redirect to index.html if root path', function(done) {

      request(app).get('/')
        .expect('location', 'index.html')
        .expect(302)
        .end(done);

    });

    it('redirect to index.html if empty path', function(done) {

      request(app).get('').end(function(err, res) {

        assert.isNull(err);
        assert.equal('index.html', res.header.location);
        assert.equal(302, res.status);

        done();

      });

    });

  });

  describe('*.html', function() {

    it('return index.html if index.html', function(done) {

      request(app).get('/index.html').end(function(err, res) {

        var indexHbs = require('../../views/index.hbs');
        var body = hbs.compile(indexHbs)({ title: 'Watcher Application' });

        var layoutHbs = require('../../views/layout.hbs');
        var html = hbs.compile(layoutHbs)({ body: body, title: 'Watcher Application' });

        assert.isNull(err);
        assert.equal(200, res.status);
        assert.include(html, res.text);

        done();

      });

    });

    it('return 404 if upsidown.html', function(done) {

      request(app).get('/upsidown.html').end(function(err, res) {

        assert.isNull(err);
        assert.equal(404, res.status);

        done();

      });

    });

  });

  describe('when the route', function() {

    var mock = {};

    before(function() {
      mock = sinon.mock(userService);
    });

    after(function() {
      mock.restore();
    });

    it('have a POST request', function(done) {

      var finalUser = { name: "teste", admin: true };
      var user = { username: "teste", password: "teste" };

      mock.expects('login').withArgs("teste", "teste").yields(null, finalUser);

      request(app).post('/').send(user).end(function(err, res) {
        assert.isNull(err);
        assert.equal(200, res.status);
        done();
      });
    });
  });
});
