'use strict';

const chai = require('chai');
const assert = chai.assert;

const mongoose = require('mongoose');
const sinon = require('sinon');
const imageService = require('../../services/imageService');
const testProperties = require('../../testProperties');
const formatError = 'Error: O formato da imagem não é válido.';

describe('Save image', function() {

  var mock = {};

  describe('With valid content', function() {

    var validImage = {
      name: "imageTest",
      content: "dGVzdGU=",
      type: "jpg",
      datetime: "2015-01-01 00:00:00"
    };

    before(function() {
      mock = sinon.mock(mongoose.model('Image'));
    });

    after(function() {
      mock.restore();
    });

    it('Return not null object', function() {

      imageService.saveImage(validImage, function(err, result) {
        assert.isNull(err);
        assert(result);
        mock.verify();
      });
    });
    it('Return correct name', function() {
      imageService.saveImage(validImage, function(err, result) {
        assert.isNull(err);
        assert.equal(validImage.name, result.name);
        mock.verify();
      });
    });
    it('Return null encode error', function() {
      imageService.saveImage(validImage, function(err, result) {
        assert.isNotNull(err);
        assert(!result);
        mock.verify();
      });
    });
  });

  describe('With invalid content', function() {

    var invalidImage = {
      name: "imageTest",
      content: "test123",
      type: "jpg",
      datetime: "2015-01-01 00:00:00"
    };

    it('Return not null Error', function() {
      imageService.saveImage(invalidImage, function(err, result) {
        assert(err);
      });
    });
    it('Return null object result', function() {
      imageService.saveImage(invalidImage, function(err, result) {
        assert.isNotNull(err);
        assert(!result);
      });
    });
  });

  describe('With invalid body', function() {

    var invalidImage = {};

    it('Return not null Error', function() {
      imageService.saveImage(invalidImage, function(err, result) {
        assert(err);
      });
    });
    it('Return null object result', function() {
      imageService.saveImage(invalidImage, function(err, result) {
        assert.isNotNull(err);
        assert(!result);
      });
    });
  });

});

describe('GetImages service', function() {

  describe('Invalid Parameteres', function() {

    it('Should return error if parameters are not sent', function() {
      imageService.getImages({}, function(err, result) {
        assert.isNotNull(err);
      });
    });

    it('Should return error if only name is sent', function() {
      imageService.getImages({ name: 'Image1' }, function(err, result) {
        assert.isNotNull(err);
      });
    });

    it('Should return error if startDate is null', function() {
      imageService.getImages({ name: 'Image1', endDate: 1475504920335 }, function(err, result) {
        assert.isNotNull(err);
      });
    });

    it('Should return error if endDate is null', function() {
      imageService.getImages({ name: 'Image1', startDate: 1475504920335 }, function(err, result) {
        assert.isNotNull(err);
      });
    });

  });

  describe('Valid Parameteres', function() {

    var mock = {};

    const validParameters = {
      name: "Image1",
      date: new Date(1475504920335)
    };

    const image = {
      name: "Image1",
      contentUrl: 'GET /image?name=Image1',
      datetime: new Date(1475504920335)
    };

    before(function() {
      mock = sinon.mock(mongoose.model('Image'));
    });

    after(function() {
      mock.restore();
    });

    it('Should return a non empty array when calling with simple date', function() {

      mock.expects('paginate').withArgs({
        name: validParameters.name,
        datetime: validParameters.date
      }, { offset: 0, limit: 10 }).yields(null,
         { docs: [image], total: 1, limit: 10, offset: 0 }
      );

      var parameters = {
        name: validParameters.name,
        date: validParameters.date,
        limit: 10
      };

      imageService.getImages(parameters, function(err, result) {

        assert.isNull(err);
        assert.isNotNull(result);
        assert.equal(result.docs.length, 1);
        assert.equal(result.docs[0].name, validParameters.name);
        assert.equal(result.docs[0].content, undefined);
        assert.isNotNull(result.docs[0].contentUrl);

      });

    });

  });

  describe('Valid Pagination Parameteres', function() {

    var mock = {};

    const validParameters = {
      name: "Image1",
      date: new Date(1475504920335)
    };

    const image = {
      name: "Image1",
      contentUrl: 'GET /image?name=Image1',
      datetime: new Date(1475504920335)
    };

    before(function() {
      mock = sinon.mock(mongoose.model('Image'));
    });

    after(function() {
      mock.restore();
    });

    it('Should return a non empty array when calling with simple date', function() {

      mock.expects('paginate').withArgs({
        name: validParameters.name,
        datetime: validParameters.date
      }, { offset: 0, limit: 10 }).yields(null,
         { docs: [image], total: 1, limit: 10, offset: 0 }
      );

      imageService.getImages({
        name: validParameters.name,
        date: validParameters.date,
        limit: 10,
        offset: 0
      }, function(err, result) {

        assert.isNull(err);
        assert.isNotNull(result);
        assert.equal(result.docs.length, 1);
        assert.equal(result.docs[0].name, validParameters.name);
        assert.equal(result.docs[0].content, undefined);
        assert.isNotNull(result.docs[0].contentUrl);

      });

    });

  });

});

describe('GetImageById service', function() {

  describe('Invalid Parameteres', function() {

    var mock = {};

    const invalidParameters = "41224d776a326fb40f000001";

    before(function() {
      mock = sinon.mock(mongoose.model('Image'));
      mock.expects('findById').withArgs(invalidParameters).yields(null, null);
    });

    after(function() {
      mock.restore();
    });

    it('Should return error if parameters are incorrect', function() {
      imageService.getImageById("1", function(err, result) {
        assert.isNotNull(err);
      });
    });

    it('Should return message if parameters are correct but the image dont exist', function() {
      imageService.getImageById("41224d776a326fb40f000001", function(err, result) {
        assert.equal(err.message, 'Nenhuma imagem com id 41224d776a326fb40f000001 foi encontrada');
      });
    });
  });

  describe('Valid Parameteres', function() {

    var mock = {};

    const validParameters = "41224d776a326fb40f000001";

    const image = {
      name: "Image1",
      contentUrl: 'GET /image?name=Image1',
      id: '41224d776a326fb40f000001',
      datetime: new Date(1475504920335)
    };

    before(function() {
      mock = sinon.mock(mongoose.model('Image'));
      mock.expects('findById').withArgs(validParameters).yields(null, [image]);
    });

    after(function() {
      mock.restore();
    });

    it('Should return a non empty array when calling with content Url', function() {

      imageService.getImageById(validParameters, function(err, result) {

        assert.isNull(err);
        assert.isNotNull(result);
        assert.equal(result[0].name, image.name);
        assert.equal(result[0].datetime, image.datetime);
        assert.equal(result[0].content, image.content);
        assert.isNotNull(result[0].contentUrl);

      });

    });

  });

});

describe('Get image content by id', function() {

  const imageId = '41224d776a326fb40f000001';
  const contentBase64 = testProperties.base64Image;

  const validImage = {
    _id: '41224d776a326fb40f000001',
    name: '41224d776a326fb40f000001',
    content: contentBase64,
    datetime: new Date()
  };

  var error = "";

  before(function() {
    sinon.stub(mongoose.model('Image'), 'findById').yields(error, validImage);
  });

  it('Should return error if image is sent without extension', function() {

    const imageWithoutExtension = {
      id: 'image'
    };

    imageService.getImageContentById(imageWithoutExtension, function(err, result) {
      assert.isNotNull(err);
      assert.isTrue(err.toString().indexOf(formatError) !== -1);
    });

  });

  it('Should return error if image is sent without extension', function() {

    const imageWithInvalidExtension = {
      id: 'image',
      format: 'piu'
    };

    imageService.getImageContentById(imageWithInvalidExtension, function(err, result) {
      assert.isNotNull(err);
      assert.isTrue(err.toString().indexOf(formatError) !== -1);
    });

  });

  it('Should return error if image is was not sent', function() {

    imageService.getImageContentById({}, function(err, result) {
      assert.isNotNull(err);
      assert.isTrue(err.toString().indexOf('Error: O id da imagem não foi enviado') !== -1);
    });

  });

  it('Should return a correct binary if png image is sent', function(done) {
    var parameters = {
      id: imageId,
      format: 'png'
    };
    imageService.getImageContentById(parameters, function(err, result) {

      assert.isNull(err);
      assert.isNotNull(result);
      assert.isNotNull(result.image);
      assert.isNotNull(result.extension);
      assert.equal('png', result.extension);
      done();
    });

  });

  it('Should return a correct binary if jpg image is sent', function(done) {
    var parameters = {
      id: imageId,
      format: 'jpg'
    };

    imageService.getImageContentById(parameters, function(err, result) {

      assert.isNull(err);
      assert.isNotNull(result);
      assert.isNotNull(result.image);
      assert.isNotNull(result.extension);
      assert.equal('jpg', result.extension);
      done();
    });

  });

});
