'use strict';

const CI = process.env.NODE_ENV === 'build';

const gulp = require('gulp');
const mocha = require('gulp-mocha');
const istanbul = require('gulp-istanbul');
const eslint = require('gulp-eslint');
const complexity = require('gulp-complexity');

const sourceFiles = ['routes/**/*.js', 'models/**/*.js', 'libs/**/*.js', 'services/**/*.js', 'middleware/**/*.js'];
const styleFiles = ['**/*.js', '!node_modules/**', '!public/js/vendor/**', '!public/css/vendor/**', '!coverage/**'];
const testFiles = ['test/**/*.js'];

gulp.task('pre-test', function() {
  return gulp.src(sourceFiles)
    // Covering files
    .pipe(istanbul({
      includeUntested: true
    }))
    // Force `require` to return covered files
    .pipe(istanbul.hookRequire());
});

gulp.task('test', ['pre-test'], function() {
  return gulp.src(testFiles)
    // Run test and reports after tests ran
    .pipe(mocha({
      reporter: CI ? 'mocha-junit-reporter' : 'spec',
      reporterOptions: {
        mochaFile: './shippable/testresults/result.xml'
      }
    }))
    // Creating coverage reports after tests ran
    .pipe(istanbul.writeReports({
      reporters: CI ? ['cobertura'] : ['text', 'text-summary', 'lcov'],
      dir: CI ? './shippable/codecoverage' : './coverage'
    }))
    // Enforce a coverage of at least 90%
    .pipe(istanbul.enforceThresholds({ thresholds: { global: 50 } }))
    .once('error', () => {
      process.exit(1);
    })
    .once('end', () => {
      process.exit();
    });
});

gulp.task('lint', function() {
  return gulp.src(styleFiles)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(eslint.result(function(result) {
      console.error('     ' + result.filePath);
    }));
});

gulp.task('complexity', function() {
  return gulp.src(styleFiles)
    .pipe(complexity({
      breakOnErrors: true,
      verbose: true
    }));
});

gulp.task('check', ['lint']);
gulp.task('default', ['test', 'check']);
