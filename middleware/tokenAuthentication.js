'use strict';

var superSecret = "";

var jwt = require('jsonwebtoken');
var userService = require('../services/userService');

function isAuthenticated(req, res, next) {

  var token = req.body.token || req.param('token') || req.headers['x-access-token'];

  if (!superSecret) {
    superSecret = "super_secret";
  }

  if (token) {

    jwt.verify(token, superSecret, function(err, decoded) {
      if (err) {
        return res.status(401).send({
          message: 'Falha na autenticação'
        });
      }
      req.decoded = decoded;

      userService.findUserByUsername(decoded.username, function(error, user) {
        if (!user) {
          return res.status(401).send({
            message: 'Falha na autenticação: usuario nao encontrado'
          });
        }
      });

      next();
    });

  } else {
    return res.status(403).send({
      message: 'Nenhum token fornecido.'
    });
  }
}
module.exports.isAuthenticated = isAuthenticated;
