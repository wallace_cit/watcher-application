'use strict';

const mongoose = require('mongoose');
const defs = require('./definitions');

const schema = new mongoose.Schema({

  username: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: [true, 'Username is required']
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    bcrypt: true
  },
  admin: defs.requiredBoolean

}, defs.options);

// schema.plugin(require('mongoose-bcrypt'));

module.exports = mongoose.model('User', schema);
