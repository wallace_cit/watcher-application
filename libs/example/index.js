'use strict';

const fs = require('fs');

// https://github.com/mattdesl/module-best-practices
// http://justbuildsomething.com/node-js-best-practices/
// http://stackoverflow.com/questions/17936515/best-practice-for-structuring-libraries-to-be-required-in-node-js

function privateFunction(file, callback) {

  fs.exists(file, function(exists) {
    var text = exists ? 'exists!' : 'do not exists!';
    return callback(exists, text);
  });

}

function publicFunction(file, callback) {
  privateFunction(file, callback);
}

exports.checkExistence = publicFunction;
