'use strict';

const users = require('express')();
const User = require('../models/user');
const tokenAuthentication = require('../middleware/tokenAuthentication');

users.all('*', tokenAuthentication.isAuthenticated);

users.route('/')

.get(function(req, res, next) {

  User.find({}, function(err, result) {
    if (err || !result) return next(err);
    if (result.length === 0) return res.sendStatus(204);
    res.json(result);
  });

})

.post(function(req, res, next) {

  User.create(req.body, function(err, result) {
    if (err || !result) return next(err);
    res.json(result);
  });

})

.delete(function(req, res, next) {

  User.remove({}, function(err, result) {
    if (err || !result) return next(err);
    res.sendStatus(204);
  });

});

users.route('/:userId')

.get(function(req, res, next) {

  User.findById(req.params.userId, function(err, result) {
    if (err || !result) return next(err);
    res.json(result);
  });

})

.put(function(req, res, next) {

  User.findByIdAndUpdate(req.params.userId, { $set: req.body }, { new: true }, function(err, result) {
    if (err || !result) return next(err);
    res.json(result);
  });

})

.delete(function(req, res, next) {

  User.findByIdAndRemove(req.params.userId, function(err, result) {
    if (err || !result) return next(err);
    res.json(result);
  });

});

module.exports = users;
