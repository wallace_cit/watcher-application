'use strict';

const images = require('express')();
const imageService = require('../services/imageService');

images.route('/')

.post(function(req, res, next) {
  imageService.saveImage(req.body, function(err, result) {
    if (err) return next(err);
    res.send(result);
  });
})

.get(function(req, res, next) {
  imageService.getImages(req.query, function(err, result) {
    if (err) return next(err);
    res.send(result);
  });

});

images.route('/:id')

.get(function(req, res, next) {

  imageService.getImageById(req.params.id, function(err, result) {
    if (err) return next(err);
    res.send(result);
  });

});

images.route('/:id/:format')

.get(function(req, res, next) {

  imageService.getImageContentById(req.params, function(err, result) {

    if (err) return next(err);

    res.status(200).setHeader('content-type', 'image/' + result.extension);
    res.end(result.image, 'binary');

  });

});

module.exports = images;
