'use strict';

const authentication = require('express')();
const tokenService = require('../services/tokenService');

authentication.route('/')

  .get(function(req, res, next) {
    // Essa validacao nao estava funcionando no teste do mocha
    // req.assert('username', 'Field name is mandatory.').notEmpty();

    tokenService.authenticate(req.query.username, req.query.password, function(err, result) {
      if (err) {
        return next(err);
      }
      req.session.username = req.query.username;
      req.session.token = result;
      res.send(result);
    });

  });

module.exports = authentication;
